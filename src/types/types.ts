import { MouseEventHandler } from 'react';

export interface Item {
  image: string;
}

export interface Params {
  item: Item;
  callback: MouseEventHandler<HTMLButtonElement>;
}
