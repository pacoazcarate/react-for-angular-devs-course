import React, { useRef, useCallback, useState } from 'react';
import Child from '../child/child';

function useForceUpdate() {
  const [value, setValue] = useState(0);
  return () => setValue((value) => value + 1);
}

export default function Parent() {
  const [item, setItem] = useState({ image: '' });
  const renderCount = useRef(0);
  renderCount.current = renderCount.current + 1;
  const forceUpdate = useForceUpdate();

  return (
    <>
      <span>Parent renders: {String(renderCount.current)}</span>
      <Child item={item} callback={useCallback(() => forceUpdate(), [])} />
    </>
  );
}
