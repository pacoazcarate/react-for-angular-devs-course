import React, { useRef } from 'react';
import { Params } from '../../types/types';
import './child.css';

function Child(prop: Params) {
  const renderCount = useRef(0);
  renderCount.current = renderCount.current + 1;
  return (
    <div>
      <span className="childRender">
        Child renders: {String(renderCount.current)}
      </span>
      <img src={prop.item.image} />
      <button onClick={prop.callback}>Re-render parent</button>
    </div>
  );
}

export default React.memo(Child);
