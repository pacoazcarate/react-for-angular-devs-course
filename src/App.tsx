import React from 'react';
import Parent from './components/parent/parent';
import './App.css';

const App: React.FC = () => (
  <div className="App">
    <header className="App-header">
      <Parent />
    </header>
  </div>
);

export default App;
